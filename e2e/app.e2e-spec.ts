import { AppPage } from './app.po';
import {Response, User} from '../src/app/Interfaces/data-interfaces';

describe('testingtime App', () => {
  let page: AppPage;
  const  mockUser1: User = {
    id: 'tHVSbtNpmnZWSLzn@gmail.com',
    email: 'tHVSbtNpmnZWSLzn@gmail.com'
  };
  const  mockUser2: User = {
    id: 'yBxUdhrLHSFseQSQ@gmail.com',
    email: 'yBxUdhrLHSFseQSQ@gmail.com'
  };
  const mockResponse1: Response = {
    answeredIndex: 1,
    userId: mockUser1.id,
    questionId: '5',
  };

  const mockResponse2: Response = {
    answeredIndex: 0,
    userId: mockUser1.id,
    questionId: '6',
  };

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display report page', () => {
    page.navigateTo('report');
    expect(page.getFirstElementOfReportList()).toEqual('0');
  });

  it('should display correct report value after send form', () => {
    page.navigateTo('form');
    page.setMail(mockUser1.email);
    page.selectResponse(mockResponse1.questionId, mockResponse1.answeredIndex);
    page.selectResponse(mockResponse2.questionId, mockResponse2.answeredIndex);
    page.send();
    page.goToReport();
    expect(page.getFirstElementOfReportList()).toEqual('1');

  });
});
