import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(path = '') {
    return browser.get('/' + path);
  }

  getFirstElementOfReportList() {
    return element(by.css('ul.list-group li.list-group-item-success span')).getText();
  }

  setMail(mail) {
    element(by.css('input[name=email]')).sendKeys(mail);

  }

  selectResponse(question, response) {
    element(by.xpath(`//select[@name=${question}]//option[@value=${response}]`)).click();
  }


  send() {
    element(by.css('.btn.btn-success.btn-lg')).click();
  }

  goToReport() {
    element(by.xpath('//nav[@class="navbar navbar-inverse"]/ul/li[2]')).click();
  }
}
