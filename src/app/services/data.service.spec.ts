import { TestBed, inject } from '@angular/core/testing';

import { DataService } from './data.service';
import {Data} from '@angular/router';
import {User, Response} from '../Interfaces/data-interfaces';

describe('DataService', () => {
  const  mockUser: User = {
    id: 'tHVSbtNpmnZWSLzn@gmail.com',
    email: 'tHVSbtNpmnZWSLzn@gmail.com'
  };
  const mockResponse: Response = {
    answeredIndex: 1,
    userId: mockUser.id,
    questionId: '1',
  };
   const mockDuplicatedResponse: Response = {
    answeredIndex: 2,
    userId: mockUser.id,
    questionId: '2',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataService]
    });
  });

  it('Should be created', inject([DataService], (service: DataService) => {
    expect(service).toBeTruthy();
  }));

  it('Adding resposes for new user should return true and add response', inject([DataService], (dataService: DataService) => {
    const response = dataService.addResponse(mockUser.email, Number(mockResponse.questionId), mockResponse.answeredIndex);
    expect(response).toBe(true);
    expect(dataService.getResponses()).toContain({userId: String(mockUser.email), questionId: String(mockResponse.questionId), answeredIndex: mockResponse.answeredIndex});
  }));

  it('Add existing response for existing user should return error message and not add duplicated value', inject([DataService], (dataService: DataService) => {
    const response = dataService.addResponse(mockUser.email, Number(mockResponse.questionId), mockResponse.answeredIndex);
    expect(response).not.toBe(true);
    expect(response).toEqual(jasmine.any(String));
    expect(dataService.getResponses()).toContain({userId: String(mockUser.email), questionId: String(mockResponse.questionId), answeredIndex: mockResponse.answeredIndex});
    expect(dataService.getResponses()).not.toContain({userId: String(mockUser.email), questionId: String(mockDuplicatedResponse.questionId), answeredIndex: mockDuplicatedResponse.answeredIndex});
  }));
});
