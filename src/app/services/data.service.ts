import {Injectable} from '@angular/core';
import {IQuestion, IUser, IResponse} from '../models/model';
import {QUESTIONS, USERS, RESPONSES} from '../mocks/mock-data';
import {InMemoryDbService} from 'angular-in-memory-web-api';


@Injectable()
export class DataService {
  constructor() {
  }

  getQuestions(): IQuestion[] {
    return QUESTIONS;
  }

  getUsers(): IUser[] {
    return USERS;
  }

  getResponses(): IResponse[] {
    return RESPONSES;
  }

  addResponse(userMail: string, questionId: number, answeredIndex: number): string | boolean {
    if ((userMail !== undefined && userMail != null) && (questionId !== undefined && questionId != null) && (answeredIndex !== undefined && answeredIndex != null)) {
      const userExists: boolean | undefined | IUser = USERS.find(x => {
        return userMail === x.email;
      });

      const responseExists: boolean | undefined | IResponse = RESPONSES.find(x => {
        return ((userMail === x.userId) && (String(questionId) === x.questionId));
      });
      if (userExists) {
      } else {
        USERS.push({id: userMail, email: userMail});
      }
      if (responseExists) {
        return 'Existing response for user';
      } else {
        RESPONSES.push({userId: String(userMail), questionId: String(questionId), answeredIndex: answeredIndex});
        console.log(RESPONSES);
        return true;
      }
    } else {
      return 'No valid data provided';
    }
  }
}
