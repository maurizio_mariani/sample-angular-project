import {IQuestion, IResponse, IUser} from '../models/model';

export const QUESTIONS: IQuestion[] = [
  { id: '5', question: 'Which smartphone do you own?', answers: ['Android', 'Iphone', 'Windows', 'Other'] },
  { id: '6', question: 'How do you travel most regularly?', answers: ['By car', 'By public transport', 'Other'] },
];

export const USERS: IUser[] = [
  {id: 'test1@gmail.com', email: 'test1@gmail.com'},
  {id: 'test2@gmail.com', email: 'test2@gmail.com'},
  {id: 'test3@gmail.com', email: 'test3@gmail.com'}
];
export const RESPONSES: IResponse[] = [
  {userId: 'test1@gmail.com', questionId: '5', answeredIndex: 2},
  {userId: 'test1@gmail.com', questionId: '6', answeredIndex: 1},
  {userId: 'test2@gmail.com', questionId: '5', answeredIndex: 3},
  {userId: 'test2@gmail.com', questionId: '6', answeredIndex: 2},
  {userId: 'test3@gmail.com', questionId: '6', answeredIndex: 1},
  {userId: 'test3@gmail.com', questionId: '5', answeredIndex: 2},
];

