export interface Question {
  id: string;
  question: string;
  answers: string[];
}

export interface User {
  id: string;
  email: string;
}

export interface Response {
  userId: string;
  questionId: string;
  answeredIndex: number;
}
