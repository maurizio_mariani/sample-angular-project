import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormComponent} from '../components/form/form.component';
import {UsersComponent} from '../components/users/users.component';
import {ReportComponent} from '../components/report/report.component';

const routes: Routes = [
  {path: '', redirectTo: '/form', pathMatch: 'full'},
  {path: 'form', component: FormComponent},
  {path: 'report', component: ReportComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }
