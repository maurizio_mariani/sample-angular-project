import { Component, OnInit } from '@angular/core';
import { DataService} from '../../services/data.service';
import {IQuestion, IResponse, IUser} from '../../models/model';
import {ViewChildren} from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {
  activeEmail: string;
  activeResponses: {id: any, value: any}[]= [];
  defaultValue = undefined;

  questions: IQuestion[];
  users: IUser[];
  responses: IResponse[];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getData();
  }

  private getData(): void {
    this.questions = this.dataService.getQuestions();
    this.users = this.dataService.getUsers();
    this.responses = this.dataService.getResponses();
  }

  setEmail(value) {
    this.activeEmail = value;
  }

  setResponse(index, value) {
    this.activeResponses.push({id: index, value: value});
  }

  save($event, form): void {
    $event.preventDefault();
    this.activeResponses.map((value, i) => {
      const result: boolean | string = this.dataService.addResponse(this.activeEmail, value.id, value.value);
      if (result === true) {
        console.log('NO error');
      } else {
        console.log(result);
      }
    });
    this.activeEmail = '';
    this.activeResponses.length = 0;
    this.defaultValue = undefined;
    form.reset();
  }
}
