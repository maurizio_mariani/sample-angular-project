import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { ReportComponent } from './report.component';
import {DataService} from '../../services/data.service';
import {Data} from '@angular/router';
import {User, Response} from '../../Interfaces/data-interfaces';

describe('ReportComponent', () => {
  let component: ReportComponent;
  let fixture: ComponentFixture<ReportComponent>;
  const  mockUser1: User = {
    id: 'tHVSbtNpmnZWSLzn@gmail.com',
    email: 'tHVSbtNpmnZWSLzn@gmail.com'
  };
  const  mockUser2: User = {
    id: 'yBxUdhrLHSFseQSQ@gmail.com',
    email: 'yBxUdhrLHSFseQSQ@gmail.com'
  };
  const mockResponse1: Response = {
    answeredIndex: 1,
    userId: mockUser1.id,
    questionId: '5',
  };

  const mockResponse2: Response = {
    answeredIndex: 0,
    userId: mockUser1.id,
    questionId: '6',
  };
  const mockResponse3: Response = {
    answeredIndex: 2,
    userId: mockUser2.id,
    questionId: '5',
  };
  const mockResponse4: Response = {
    answeredIndex: 2,
    userId: mockUser2.id,
    questionId: '6',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportComponent ],
      providers: [DataService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('getMultipleCount should return 1 ', inject([DataService], (dataService: DataService) => {
    dataService.addResponse(mockUser1.email, Number(mockResponse1.questionId), mockResponse1.answeredIndex);
    dataService.addResponse(mockUser1.email, Number(mockResponse2.questionId), mockResponse2.answeredIndex);
    expect(component.getMultipleCount([{responseIndex: mockResponse1.answeredIndex, questionId: mockResponse1.questionId}, {responseIndex: mockResponse2.answeredIndex, questionId: mockResponse2.questionId}])).toEqual(1);
  }));
  it('getMultipleCount should still return 1 ', inject([DataService], (dataService: DataService) => {
    dataService.addResponse(mockUser2.email, Number(mockResponse3.questionId), mockResponse3.answeredIndex);
    dataService.addResponse(mockUser2.email, Number(mockResponse4.questionId), mockResponse4.answeredIndex);
    expect(component.getMultipleCount([{responseIndex: mockResponse2.answeredIndex, questionId: mockResponse2.questionId}, {responseIndex: mockResponse2.answeredIndex, questionId: mockResponse2.questionId}])).toEqual(1);
  }));

});
