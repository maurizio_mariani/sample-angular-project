import {Component, DoCheck, OnChanges, OnInit} from '@angular/core';
import {DataService} from '../../services/data.service';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/observable/of';
import {Input} from '@angular/core';
import {SimpleChanges} from '@angular/core';
import {IQuestion, IResponse, IUser} from '../../models/model';
import {RESPONSES} from '../../mocks/mock-data';
import * as _ from 'underscore';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  questions: IQuestion[];
  users: IUser[];
  responses: IResponse[];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.questions = this.dataService.getQuestions();
    this.users = this.dataService.getUsers();
    this.responses = this.dataService.getResponses();
  }


  getCount(responseIndex, questionsId): number {
    let count = 0;

    this.responses.map(x => {
      if (x.answeredIndex == responseIndex && x.questionId === questionsId) {
        count++;
      }
    });
    return count;
  }

  getMultipleCount(responses: Array<{responseIndex: number; questionId: number | string}>) {
    function recoursiveIntersection(set) {
      if (set.length > 0) {
        return _.intersection(set.shift(), set.shift());
      } else {
        return set;
      }
    }

    const users = [];
    responses.map(x => {
      const tempUsers: string[] = [];
      this.responses.map(y => {
        if ((x.responseIndex == y.answeredIndex) && (y.questionId == x.questionId)) {
          tempUsers.push(y.userId);
        }
      });
      if (tempUsers.length > 0) {
            users.push(tempUsers);
        }
    });

    return recoursiveIntersection(users).length;
  }
}
