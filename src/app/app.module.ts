import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ReportComponent } from './components/report/report.component';
import { FormComponent } from './components/form/form.component';
import { UsersComponent } from './components/users/users.component';
import { AppRoutingModule } from './routers/app-routing.module';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import {DataService} from './services/data.service';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NavbarComponent } from './components/navbar/navbar.component'; // <-- NgModel lives here



@NgModule({
  declarations: [
    AppComponent,
    ReportComponent,
    FormComponent,
    UsersComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
