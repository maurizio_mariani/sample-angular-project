import {Question, User, Response} from '../Interfaces/data-interfaces';

export class IQuestion implements Question {
  id: string;
  question: string;
  answers: string[];
}

export class IUser implements User {
  id: string;
  email: string;
}

export class IResponse implements Response {
  userId: string;
  questionId: string;
  answeredIndex: number;
}
